FROM docker.elastic.co/beats/metricbeat:5.4.0
MAINTAINER stoleas

ADD metricbeat.yml /usr/share/metricbeat/metricbeat.yml